import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

/**
 * Created by YevgeniyS on 07.03.2016.
 */
public class SimpleTest {

    public static void main(String[] args) throws InterruptedException {
        System.out.println("openingOfNewsTabTest:");
        openingOfNewsTabTest();                     //checking of opening the News tab
        System.out.println("openingOfSportTabTest:");
        openingOfSportTabTest();                    //checking of opening the Sport tab
        System.out.println("openingOfWeatherTabTest:");
        openingOfWeatherTabTest();                  //checking of opening the Weather tab
        System.out.println("openingOfShopTabTest:");
        openingOfShopTabTest();                     //checking of opening the Shop tab
        System.out.println("checkThatFindAForecastElementPresentsTest:");
        checkThatFindAForecastElementPresentsTest();            //check that "Find a Forecast" element presents
    }

    private static void checkThatFindAForecastElementPresentsTest() throws InterruptedException {
        WebDriver driver = new FirefoxDriver();
        driver.get("http://www.bbc.com");
        WebElement newsTabLink = driver.findElement(By.xpath("//div[@id='orb-nav-links']//li/a[text()='Weather']"));
        newsTabLink.click();
  //      Thread.sleep(3000);
        WebElement findAForecastFrame = driver.findElement(By.id("find-a-forecast"));
        boolean expectedResult = true;
        boolean actualResult = findAForecastFrame.isDisplayed();
        compareExpectedAndActualResults(expectedResult,actualResult);
        driver.quit();
    }

    private static void openingOfShopTabTest() {
        WebDriver driver = new FirefoxDriver();     //open FF browser
        driver.get("http://www.bbc.com");           //navigate to URL
        WebElement newsTabLink = driver.findElement(By.xpath("//div[@id='orb-nav-links']//li/a[text()='Shop']"));   //find element on page
        newsTabLink.click();                        //click on element
        String expectedResult = "http://www.bbcshop.com/";  //specification of expected page
        String actualResult = driver.getCurrentUrl();       //getting current page
        compareExpectedAndActualResults(expectedResult,actualResult);  //comparing expected and actual results
        driver.quit();                                      //close FF browser
    }

    private static void openingOfWeatherTabTest() {
        WebDriver driver = new FirefoxDriver();
        driver.get("http://www.bbc.com");
        WebElement newsTabLink = driver.findElement(By.xpath("//div[@id='orb-nav-links']//li/a[text()='Weather']"));
        newsTabLink.click();
        String expectedResult = "http://www.bbc.com/weather/";
        String actualResult = driver.getCurrentUrl();
        compareExpectedAndActualResults(expectedResult,actualResult);
        driver.quit();
    }

    private static void openingOfSportTabTest() {
        WebDriver driver = new FirefoxDriver();
        driver.get("http://www.bbc.com");
        WebElement newsTabLink = driver.findElement(By.xpath("//div[@id='orb-nav-links']//li/a[text()='Sport']"));
        newsTabLink.click();
        String expectedResult = "http://www.bbc.com/sport";
        String actualResult = driver.getCurrentUrl();
        compareExpectedAndActualResults(expectedResult,actualResult);
        driver.quit();
    }

    private static void openingOfNewsTabTest() {
        WebDriver driver = new FirefoxDriver();
        driver.get("http://www.bbc.com");
        WebElement newsTabLink = driver.findElement(By.xpath("//div[@id='orb-nav-links']//li/a[text()='News']"));
        newsTabLink.click();
        String expectedResult = "http://www.bbc.com/news";
        String actualResult = driver.getCurrentUrl();
        compareExpectedAndActualResults(expectedResult,actualResult);
        driver.quit();
    }
    private static void compareExpectedAndActualResults(String expectedResult, String actualResult){
        if (expectedResult.equals(actualResult)){           //comparing  expected and actual pages
            System.out.println("Passed.");                  //if statement is true then print "Passed"
        }else{
            System.out.println("Failed. Expected result: " + expectedResult + "\n"
                    + "Actual result: " + actualResult);    //if statement is false then print "Failed" with difference between results
        }
    }
    private static void compareExpectedAndActualResults(boolean expectedResult, boolean actualResult){
        if (expectedResult == actualResult){           //comparing  expected and actual pages
            System.out.println("Passed.");                  //if statement is true then print "Passed"
        }else{
            System.out.println("Failed. Expected result: " + expectedResult + "\n"
                    + "Actual result: " + actualResult);    //if statement is false then print "Failed" with difference between results
        }
    }

}
